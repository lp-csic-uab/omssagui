OmssaGui vs 1.2

(c) Copyright 2010-2014 Joaqu�n Abian 
    at Laboratori de Proteomica CSIC/UAB <lp.csic@uab.cat>
    http://proteomica.uab.cat


OmssaGui
A front-end (or GUI) for the OMSSA search engine.
This way, OmssaGui allows to easily use OMSSA CLI (omssacl.exe) to the identification of MS/MS spectra from MGF (Mascot Generic Format) files.
The output of searches is saved as a XLS spreadsheet-like file.

@TO-DO


*** OmssaGui Licence ***

      This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 3 of the License, or
      (at your option) any later version.
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software

      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
      MA 02110-1301, USA.

See LICENCE.txt for more details about licensing of the software (GPL v3).


