---

**WARNING!**: This is the *Old* source-code repository for OmssaGui program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/omssagui/) located at https://sourceforge.net/p/lp-csic-uab/omssagui/**  

---  
  
  
![https://lh4.googleusercontent.com/-ChZlggj82DQ/UfKKHfggsnI/AAAAAAAAAVs/JyDJXRl_Q04/s800/omssagui.png](https://lh4.googleusercontent.com/-ChZlggj82DQ/UfKKHfggsnI/AAAAAAAAAVs/JyDJXRl_Q04/s800/omssagui.png)


---

**WARNING!**: This is the *Old* source-code repository for OmssaGui program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/omssagui/) located at https://sourceforge.net/p/lp-csic-uab/omssagui/**  

---  
  
  
**Table Of Contents:**

[TOC]

#### Description
**`OmssaGui`** is a [Perl](http://en.wikipedia.org/wiki/Perl) application that acts as a [front-end](http://en.wikipedia.org/wiki/Front_and_back_ends#Computer_science) (or [GUI](http://en.wikipedia.org/wiki/Graphical_user_interface)) for the [OMSSA](http://pubs.acs.org/doi/abs/10.1021/pr0499491) search engine.

This way, `OmssaGui` allows to easily use OMSSA [CLI](http://en.wikipedia.org/wiki/Command-line_interface) (omssacl.exe) to the identification of MS/MS spectra from MGF (Mascot Generic Format) files.

The output of searches is saved as a [XLS](http://en.wikipedia.org/wiki/Microsoft_Excel_file_format#File_formats) spreadsheet-like file.

![https://lh5.googleusercontent.com/-KMXnsECbZbQ/UfJ8IwuXwLI/AAAAAAAAAVc/1UNpzrLrlHU/s800/29_OmssaGui_MS3.png](https://lh5.googleusercontent.com/-KMXnsECbZbQ/UfJ8IwuXwLI/AAAAAAAAAVc/1UNpzrLrlHU/s800/29_OmssaGui_MS3.png)

#### Installation

Tested in Windows XP 32 bits and Windows 7 64 bits.

##### External Requirements

  * Only the [OMSSA](ftp://ftp.ncbi.nlm.nih.gov/pub/lewisg/omssa/) search engine executable, if you use the [Windows Installer](https://googledrive.com/host/0B0ggtLTCQIP3ekU2QTRIX2stdFk/OmssaGui_1.2.0_setup.exe) (see below).

##### From Windows Installer

  1. Download `OmssaGui_x.y.z_setup.exe` [Windows Installer](https://bitbucket.org/lp-csic-uab/omssagui/downloads).
  1. [Get your Password](#markdown-header-download) for the installer.
  1. Double click on the installer and follow the Setup Wizard.
  1. Run `OmssaGui` by double-clicking on the `OmssaGui` short-cut in your desktop or from the START-PROGRAMS application folder created by the installer.

##### From Source

  1. Install [Perl](http://www.perl.org/) and other third party software indicated in [Dependencies](#markdown-header-source-dependencies), as required.
  1. Download `OmssaGui` source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/omssagui/src).
  1. Copy the `OmssaGui` folder in your path. If you only plan to run the program from a particular user, you will need only to put `OmssaGui` folder inside a user folder.
  1. Run `OmssaGui` by double-clicking on the `omssagui.pl` module, or typing  `perl omssagui.pl`  on the command line.

###### _Source Dependencies:_

  * [Perl](http://www.perl.org/) 5.16 (not tested with other versions)
  * [wxPerl](http://www.wxperl.it/) 0.9921 (with Alien::wxWidgets 0.64)

Third-party software and package versions correspond to those used for the installer available here. Lower versions have not been tested, although they may also be fine.

#### Download

You can download the Windows Installer for the last version of **`OmssaGui`** [here](https://bitbucket.org/lp-csic-uab/omssagui/downloads)[![](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg)](https://bitbucket.org/lp-csic-uab/omssagui/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program. The password is not required to run the application from source code.


---

**WARNING!**: This is the *Old* source-code repository for OmssaGui program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/omssagui/) located at https://sourceforge.net/p/lp-csic-uab/omssagui/**  

---  
  
  
