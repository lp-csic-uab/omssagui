package omssa;

use utf8;
use strict;
use warnings;
use Text::CSV_XS;
use Data::Dumper;
use File::Basename;
use Wx qw[:everything];

our $VERSION = '1.00';
our @EXPORT_OK = qw(main);
use base qw(Exporter);


sub main{
	my @mgfs = @{$_[0]};
	my %params_global = %{$_[1]};
	my @params_individual = @{$_[2]};
	my $num_mgf_files = $_[3];
	my $this = $_[4];
	my $event = $_[5];
	my @total_data;
	my $mgfs_processed = 0;
	my $out_filedir = $params_global{'out_filedir'};
    my $xml = $params_global{'XML'};
	$this->SetStatusText('Searching...');
	$this->Update();
	$this->Refresh();
	foreach my $mgf(@mgfs){
		$this->SetStatusText('Processing ' . (1+$mgfs_processed) . ' / ' . $num_mgf_files . ' mgf files');
		$this->Update();
		$this->Refresh();
		my $file_in = '"' . $mgf . '"';
		my $file_out = '"' . $mgf . '.csv' . '"';
		my $file_csv = $mgf . '.csv';

		my $database_blast_formated = $params_global{'database'};
		my $exe = $params_global{'executable'};

		my $fm = ' -fm '.$file_in;
		my $op = ' -oc '.$file_out;
		my $opx;
		if ($xml){
			#file_xml will be saved in the same folder as result report file,
			#and will have the same name as the mgf file processed to generate 
			#it preceded by a order numbre (ex: test.mgf -> 0_test.omx )
			my $mgf_name;
			my $mgf_dir;
			my $mgf_ext;
			($mgf_name, $mgf_dir, $mgf_ext) = fileparse($mgf, ('\.[^.]+$') );
			my $file_xml = '"' . $out_filedir . $mgfs_processed . '_' . $mgf_name . '.omx' . '"'; #New xml output file
	        $opx = ' -w -ox '.$file_xml; #New argument to produce also a xml output	with spectra and search params included		
		}
		my $te = ' -te '.$params_global{'tol_precursor'};
		my $to = ' -to '.$params_global{'tol_product'};


		my $zl = ' -zl '.$params_global{'min_charge'};
		my $zh = ' -zh '.$params_global{'max_charge'};
		my $v = ' -v '.$params_global{'missed_cleavages'};
		my $mf = ' -mf 3';
		my $mv;
		if($params_individual[$mgfs_processed]){
			$mv = ' -mv ' . $params_individual[$mgfs_processed];
		}
		my $d = ' -d "' . $params_global{'database'} . '"';
		my $nt = ' -nt 0';
		my $enzyme = ' -e ' . $params_global{'enzyme'};


		my @argumentos=($exe, $fm, $op, $opx, $te, $nt, $to, $zh, $zl, $v, $mf, $mv, $enzyme, $d);
		my $ejecuta = "";
		foreach(@argumentos){
			unless($_){
				next;
			}
			$ejecuta .= $_;
		}
		print $ejecuta, "\n";
		my $output = `$ejecuta`;
		$mgfs_processed++;
		if(-T$file_csv){
			print "\nEncontrado out csv\n";
			sleep 10;                        #wait 10 seconds trying to fix IO crash
			my @data = omssa::parse_omssa_csv($file_csv);

			foreach(@data){
				push @total_data, $_;
			}

			unlink $file_csv;
		}
	}

	write_report(\@total_data,$params_global{'file_out'});
	print "\n\nSEARCH FINISHED\n";
	return 1;
}

sub write_report{
	my @data = @{$_[0]};
	my $report = $_[1];
	open SAL, ">$report";

	print SAL "File\tscan 1\tscan n\tms_level\tPeptide\tE-value\tMass\tgi\tAccession\tStart\tStop\tDefline\tMods\tCharge\tTheo Mass\tP-value\n";
	for(my $i=0; $i<$#data+1; $i++){
		my @cols = split /\t/,$data[$i];
		my ($file, $scans, $ms, $rt) = split /\,/, $cols[0];
		$scans =~ s/Scan\://;
		my ($s1, $s2);
		if($scans =~ /\-/){
			($s1, $s2) = split /\-/, $scans;
		}
		else{
			($s1, $s2)=($scans, $scans);
		}
		$ms =~ s/MS\://;
		print SAL $file, "\t", $s1, "\t", $s2, "\t", $ms, "\t";
		for(my $j=1; $j<$#cols; $j++){
			print SAL $cols[$j], "\t";
		}
		print SAL "\n";
	}

	close SAL;
	return 1;

}


sub parse_omssa_csv{
	my $file = $_[0];
	my %data;
	my $csv = Text::CSV_XS->new ({ binary => 1, eol => $/ });

	open my $io, "<", $file or die "Problem: $!";
	my $lines = 0;
	while (my $row = $csv->getline ($io)) {
		unless($lines){                         #skip headers
			$lines++;
			next;
		}
		my @fields = @$row;
		my $pep = $fields[2];
		my $modc = $fields[10];
		$pep =~ tr/a-z/A-Z/;
		my @aas = split //, $pep;
		my @mods = split / \,/, $modc;
		my %site_mod;
		foreach(@mods){
			my ($mod, $site) = split /\:/,$_;
			if($mod eq 'oxidation of M'){
				$mod = 35;
			}
			if($mod =~ /phosphorylation/){
				$mod = 21;
			}
			if($mod =~ /TMT/){
				$mod = 737;
			}
			if($mod =~ /dehydro/){
				$mod=23;
			}
			if($mod =~ /iTRAQ/){
				$mod = 214;
			}

			$site_mod{$site} = $mod;
		}
		my $peptide = '';
		for(my $i=0; $i<$#aas+1; $i++){
			$peptide = $peptide.$aas[$i];
			if(defined $site_mod{($i+1)}){
				$peptide = $peptide . '(' . $site_mod{($i+1)} . ')';
			}
		}
		$fields[2] = $peptide;
		unless(defined $data{$fields[1]}){
			$data{$fields[1]} = $fields[2] . "\t" . $fields[3] . "\t" . $fields[4]."\t".$fields[5]."\t".$fields[6]."\t".$fields[7]."\t".$fields[8]."\t".$fields[9]."\t".$fields[10]."\t".$fields[11]."\t".$fields[12]."\t".$fields[13]."\t".$fields[14];
		}
	}

	my @data;
	foreach(keys %data){
		my $fila = $_ . "\t" . $data{$_};
		push @data, $fila;
	}

	return @data;
}



1;
